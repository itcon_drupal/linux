# ITCON Services Linux Docker Image

A standard CentOS 7 image tuned for developing in Drupal 6, 7, 8, PHP 7.2, MariaDB (MySQL) Client and Perl. Can be used for production or development environments.  

Only tools are provided on here and no software by default. Composer, Drush and Drupal Console are included within the container for convenience. It is recommended all be used within the container for the best results. Be sure to mount any needed file paths as volumes to ensure persistence.

#### Apache Settings, Configurations and Mount Points:  
DocumentRoot: /var/www/html/web  
Volume Mount Point: /var/www  
Webalizer: /var/www/webalizer  
Logs: /var/log/httpd  

#### Last Updated: October 20, 2019

### Environment Variables

**EXTRA_HTTPD**  
_Default Valye: NULL_  
Extra code to be placed in the appropriate httpd.conf file. Be careful. Doing this wrong will cause your container not to properly start. See example `docker-compose.yml` for more information.  

**HSTS_HEADER**  
_Default Value: NULL_  
Set to 1 (or any value) if you want to put HSTS headers in your Apache headers. Do not configure this item or set to 0 to disable.

**HSTS_PRELOAD**  
_Default Value: 0_  
Dictates whether the HSTS header should be preloadable. PLEASE USE WITH CAUTION (https://hstspreload.org). Set to 1 if you want to put HSTS headers in your confiuguration or 0 if you do not (if unset, zero is assumed).

**HSTS_TTL**  
_Default Value: 3600_  
The TTL for an HSTS configured header (1 Hour by default).  

**HTACCESS_DESCRIPTION**  
THe web site description to appear in the htaccess username/password box. This is what determines if this option is enabled. A value here enables the htaccess authentication system in Apache. Omission leaves it disabled.

**HTACCESS_PASSWORD**  
The password for the user configured in the htaccess dialog.

**HTACCESS_USERNAME**  
The username to be configured in the htaccess dialog.  

**MAILCATCHER**  
_Default Value: 0_  
Set to 1 to enable Mailcatcher on this image. Used for email debugging.  

**PHP_DISPLAY_ERRORS**  
_Default Value: Off_  
This directive controls whether or not and where PHP will output errors, notices and warnings.

**PHP_DISPLAY_STARTUP_ERRORS**  
_Default Value: Off_  
The display of errors which occur during PHP's startup sequence are handled separately from display_errors.

**PHP_MAX_EXECUTION_TIME**  
_Default Value: 300_  
Maximum execution time of each script, in seconds. A value of 0 disables the limit.

**PHP_MAX_INPUT_TIME**  
_Default Value: 300_  
Maximum amount of time each script may spend parsing request data. A value of -1 disables the limit.

**PHP_MAX_INPUT_VARS**  
_Default Value: 1000_  
How many GET/POST/COOKIE input variables may be accepted.

**PHP_MEMORY_LIMIT**  
_Default Value: 386M_  
Maximum amount of memory a script may consume.

**PHP_POST_MAX_SIZE**  
_Default Value: 256M_  
Maximum size of POST data that PHP will accept. A value of 0 disables the limit.

**PHP_UPLOAD_MAX_FILESIZE**  
_Default Value: 256M_  
Maximum allowed size for uploaded files.  

**WEBALIZER_PASSWORD**  
_Default Value: NULL_ 
The password for the Webalizer log analysis web url. Does nothing if WEBALIZER_USERNAME is not configured.  

**WEBALIZER_USERNAME**  
_Default Value: NULL_   
The username to be configured in the htaccess dialog for webalizer. If this is blank, access will not be controlled.  

**XDEBUG**  
_Default Value: NULL_  
This is a boolean. Set to 1 to enable xdebug for this container. Set to 0 or leave as default to disable. This has a performance cost so choose wisely.  
