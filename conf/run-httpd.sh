#!/bin/bash

# Add an HSTS header if configured.
if [[ -z "${HSTS_HEADER}" ]] || [[ $HSTS_HEADER = "0" ]]; then
  export HSTS_HEADER=''
else
  if [[ -n "${HSTS_TTL}" ]]; then
    export HSTS_TTL="max-age=$HSTS_TTL;"
  else
    export HSTS_TTL="max-age=3600;"
  fi

  if [[ -n "${HSTS_PRELOAD}" ]] && [[ $HSTS_PRELOAD = "1" ]]; then
    export HSTS_PRELOAD='; preload'
  else
    export HSTS_PRELOAD=''
  fi
  export HSTS_HEADER="Header always set Strict-Transport-Security \"${HSTS_TTL} includeSubDomains${HSTS_PRELOAD}\""
fi

if [[ -n "${EXTRA_HTTPD}" ]]; then
    export EXTRA_HTTPD="$EXTRA_HTTPD"
  else
    export EXTRA_HTTPD=""
fi

if [[ -n "${HTACCESS_DESCRIPTION}" ]]; then
  /usr/bin/htpasswd -cb /var/www/.htpasswd $HTACCESS_USERNAME $HTACCESS_PASSWORD
  envsubst < /etc/httpd/conf/httpd-auth.conf > /etc/httpd/conf/httpd.conf
else
  envsubst < /etc/httpd/conf/httpd-noauth.conf > /etc/httpd/conf/httpd.conf
fi

# Set our PHP ini environment variable defauts.
if [[ ! -n "${PHP_DISPLAY_ERRORS}" ]]; then
  export PHP_DISPLAY_ERRORS=Off
fi
if [[ ! -n "${PHP_DISPLAY_STARTUP_ERRORS}" ]]; then
  export PHP_DISPLAY_STARTUP_ERRORS=Off
fi
if [[ ! -n "${PHP_MAX_EXECUTION_TIME}" ]]; then
  export PHP_MAX_EXECUTION_TIME=300
fi
if [[ ! -n "${PHP_MAX_INPUT_TIME}" ]]; then
  export PHP_MAX_INPUT_TIME=300
fi
if [[ ! -n "${PHP_MAX_INPUT_VARS}" ]]; then
  export PHP_MAX_INPUT_VARS=1000
fi
if [[ ! -n "${PHP_MEMORY_LIMIT}" ]]; then
  export PHP_MEMORY_LIMIT=386M
fi
if [[ ! -n "${PHP_POST_MAX_SIZE}" ]]; then
  export PHP_POST_MAX_SIZE=256M
fi
if [[ ! -n "${PHP_UPLOAD_MAX_FILESIZE}" ]]; then
  export PHP_UPLOAD_MAX_FILESIZE=256M
fi
if [[ -n "${XDEBUG}" ]] || [[ $XDEBUG = "1" ]]; then
  export XDEBUG='/usr/lib64/php-zts/modules/xdebug.so'
else
  export XDEBUG=''
fi

envsubst < /etc/php.ini > /etc/php2.ini
mv -f /etc/php2.ini /etc/php.ini

if [[ -d "/copy-conf.d" ]]; then
  cp -rf /copy-conf.d/* /etc/httpd/conf.d/.
fi

if [[ -d "/copy-sysconfig" ]]; then
  cp -rf /copy-sysconfig/* /etc/sysconfig/.
fi

if [[ -d "/copy-conf" ]]; then
  cp -rf /copy-conf/* /etc/httpd/conf/.
fi

# Set our PHP ini environment variable defauts.
  if [[ -n "${MAILCATCHER}" ]] && [[ $MAILCATCHER = "1" ]]; then
  mailcatcher --ip 0.0.0.0
fi

# Make sure we're not confused by old, incompletely-shutdown httpd
# context after restarting the container.  httpd won't start correctly
# if it thinks it is already running.
rm -rf /run/httpd/* /tmp/httpd*

exec /usr/sbin/apachectl -DFOREGROUND